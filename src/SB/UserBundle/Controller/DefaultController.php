<?php

namespace SB\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/user")
     * @Template()
     */
    public function indexAction($name='insecure')
    {
        return array('name' => $name);
    }
	
    /**
     * @Route("/secured")
     * @Template()
     */
    public function securedAction($name='secured')
    {
        return array('name' => $name);
    }
}
