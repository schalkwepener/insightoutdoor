<?php

namespace SB\BillBoardBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('number')
            ->add('siteNumber')
            ->add('provinceId')
            ->add('city')
            ->add('suburb')
            ->add('street')
            ->add('cnrStreet')
            ->add('gpsLong')
            ->add('gpsLat')
            ->add('illuminationId')
            ->add('typeId')
            ->add('sizeId')
            ->add('categoryId')
            ->add('rateTypeId')
            ->add('displayTemplateId')
            ->add('deletedAt') 
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SB\BillBoardBundle\Entity\Product'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sb_billboardbundle_product';
    }
}
