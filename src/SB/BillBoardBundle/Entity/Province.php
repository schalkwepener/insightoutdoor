<?php

namespace SB\BillBoardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Province
 *
 * @ORM\Table(name="province")
 * @ORM\Entity
 * @ORM\Table(name="province",indexes={
 *           @ORM\Index(name="province", columns={"province"})
 *      }
 * ) 
 * 
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Province
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="province", type="string", length=255)
     */
    private $province;
    
    /**
    * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
    */
    private $deletedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="SB\BillBoardBundle\Entity\Product", mappedBy="province")
     */
    protected $product;
            

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set province
     *
     * @param string $province
     * @return Province
     */
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return string 
     */
    public function getProvince()
    {
        return $this->province;
    }
    
     public function __construct()
    {
        $this->province = new ArrayCollection();
    }

    public function __toString()
    {
         return $this->getProvince();
    }
    
    /**
     * Add example
     *
     * @param \SB\BillBoardBundle\Entity\Product $product
     * @return Colour
     */
    public function addProduct(\SB\BillBoardBundle\Entity\Product $product)
    {
        $this->product[] = $product;
    
        return $this;
    }

    /**
     * Remove example
     *
     * @param \SB\BillBoardBundle\Entity\Product $product
     */
    public function removeProduct(\SB\BillBoardBundle\Entity\Product $product)
    {
        $this->product->removeElement($product);
    }

    
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }
}
