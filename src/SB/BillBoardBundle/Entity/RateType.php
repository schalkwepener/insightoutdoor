<?php


namespace SB\BillBoardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * RateType
 *
 * @ORM\Table(name="ratetype")
 * @ORM\Entity
 * @ORM\Table(name="ratetype",indexes={
 *           @ORM\Index(name="ratetype", columns={"ratetype"})
 *      }
 * ) 
 * 
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class RateType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="ratetype", type="string", length=255)
     */
    private $rateType;
    
    /**
    * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
    */
    private $deletedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="SB\BillBoardBundle\Entity\Product", mappedBy="ratetype")
     */
    protected $product;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rateType
     *
     * @param string $rateType
     * @return RateType
     */
    public function setRateType($rateType)
    {
        $this->rateType = $rateType;

        return $this;
    }

    /**
     * Get rateType
     *
     * @return string 
     */
    public function getRateType()
    {
        return $this->rateType;
    }
    
     public function __construct()
    {
        $this->rateType = new ArrayCollection();
    }

    public function __toString()
    {
         return $this->getRateType();
    }
    
    /**
     * Add example
     *
     * @param \SB\BillBoardBundle\Entity\Product $product
     * @return Colour
     */
    public function addProduct(\SB\BillBoardBundle\Entity\Product $product)
    {
        $this->product[] = $product;
    
        return $this;
    }

    /**
     * Remove example
     *
     * @param \SB\BillBoardBundle\Entity\Product $product
     */
    public function removeProduct(\SB\BillBoardBundle\Entity\Product $product)
    {
        $this->product->removeElement($product);
    }

    
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }
}
