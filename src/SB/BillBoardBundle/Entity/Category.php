<?php

namespace SB\BillBoardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity
 * @ORM\Table(name="category",indexes={
 *           @ORM\Index(name="category", columns={"category"})
 *      }
 * ) 
 * 
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="category", type="string", length=255)
     */
    private $category;

    /**
    * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
    */
    private $deletedAt;
    
    /**
     * @ORM\OneToMany(targetEntity="SB\BillBoardBundle\Entity\Product", mappedBy="category")
     */
    protected $product;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Category
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    public function __construct()
    {
        $this->category = new ArrayCollection();
    }

    public function __toString()
    {
         return $this->getCategory();
    }
    
    /**
     * Add example
     *
     * @param \SB\BillBoardBundle\Entity\Product $product
     * @return Colour
     */
    public function addProduct(\SB\BillBoardBundle\Entity\Product $product)
    {
        $this->product[] = $product;
    
        return $this;
    }

    /**
     * Remove example
     *
     * @param \SB\BillBoardBundle\Entity\Product $product
     */
    public function removeProduct(\SB\BillBoardBundle\Entity\Product $product)
    {
        $this->product->removeElement($product);
    }

    
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

}
